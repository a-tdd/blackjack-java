package ru.agilix.blackjackkata.e2e.steps;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import io.cucumber.java.ru.Допустим;
import io.cucumber.java.ru.Когда;
import io.cucumber.java.ru.Тогда;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class DealerSteps {
    private static final String HOST = "localhost";
    private static final String PORT = "8080";
    private ArrayList<String> hand = new ArrayList<>();

    @Допустим("у дилера на руках {string}")
    public void dealerHasCards(String hand) {
        this.hand.add(hand);
    }

    @Когда("он берет {string} из колоды")
    public void dealerGetsAnotherCard(String card) {
        this.hand.add(card);
    }

    @Тогда("он не должен брать еще карту")
    public void онНеДолженБратьЕщеКарту() throws IOException {
        JsonObject json = getJSONRequest("http://"+HOST+":"+PORT+"/dealer/action?" + handToGetParams());
        JsonElement canHit = json.get("can hit");
        assertFalse(canHit.getAsBoolean());
    }

    @Тогда("он должен брать еще карту")
    public void онДолженБратьЕщеКарту() throws IOException {
        JsonObject json = getJSONRequest("http://"+HOST+":"+PORT+"/dealer/action?" + handToGetParams());

        JsonElement canHit = json.get("can hit");

        assertTrue(canHit.getAsBoolean());
    }

    @Тогда("он производит {string}")
    public void онПроизводит(String action) throws IOException {
        JsonObject json = getJSONRequest("http://"+HOST+":"+PORT+"/dealer/action?" + handToGetParams());

        JsonElement canHit = json.get("can hit");

        if(action.equals("берет")) {
            assertTrue(canHit.getAsBoolean());
        } else {
            assertFalse(canHit.getAsBoolean());
        }
    }

    private JsonObject getJSONRequest(String urlString) throws IOException {
        String result = "";
        URL url = new URL(urlString);

        Scanner scanner = new Scanner(url.openStream());
        String response = scanner.useDelimiter("\\Z").next();
        JsonObject jsonObject = JsonParser.parseString(response).getAsJsonObject();
        scanner.close();

        return jsonObject;
    }

    private String handToGetParams() {
        return this.hand.stream().map(c -> "card=" + c).reduce("", (x, y) -> x + "&" + y);
    }

}
